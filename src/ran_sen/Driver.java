package ran_sen;

public class Driver {
	
	public static void main(String[] args)
	{

		WordList wordList = new WordList();
		
		SentenceBuilder someBuilder = new SentenceBuilder(wordList);
		
		for(int i = 0; i < 100; i++) {
			String result = someBuilder.generateSentence();
			
			System.out.println(result);
		}
	
	}

}
