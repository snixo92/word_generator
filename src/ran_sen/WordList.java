package ran_sen;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class WordList {
	
	public List<String> nouns;
	public List<String> adverbs;
	public List<String> adjectives;
	public List<String> verbs;
	public List<String> article;
	public List<String> pro;
	
	public String[] art = new String[4];
	// ~~~~~~~~~~~~~~~~~~Constructors~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	WordList(){
		nouns = initializeList("noun.txt");
		adverbs = initializeList("adv.txt");
		adjectives = initializeList("adj.txt");
		verbs = initializeList("verb.txt");
		pro = initializeList("pronoun.txt");
		
		art[0] = "the";
		art[1] = "that";
		art[2] = "a";
		art[3] = "an";
	}
	
	
	public List<String> initializeList(String file_name) {
		
		ArrayList<String> arr = new ArrayList<String>();
        try (BufferedReader br = new BufferedReader(new FileReader(file_name)))
        {

            String sCurLine;

            while ((sCurLine = br.readLine()) != null) {
                arr.add(sCurLine);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } 
        
        
		return arr;
	}
	
	

}