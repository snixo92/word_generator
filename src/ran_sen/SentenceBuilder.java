package ran_sen;

import java.util.List;

public class SentenceBuilder {
	
	public WordList wl;
	
	SentenceBuilder(WordList someWordList){
		wl = someWordList;
		
	}
	
	// generates a string of format:
	// [article or pronoun] adjective1 noun1 adverb verb [article2 or pronoun2] adjective2 noun2
	public String generateSentence() {
		
		String sentence;
		List<String> allNouns = wl.nouns;
		List<String> allAdjectives = wl.adjectives;
		List<String> allVerbs = wl.verbs;
		List<String> allAdverbs = wl.adverbs;
		List<String> allPro = wl.pro;
		String[] article = wl.art;

		int rNum = getRandomIntegerBetweenRange(0, allNouns.size());
		int rAdj = getRandomIntegerBetweenRange(0, allAdjectives.size());
		int rVerb = getRandomIntegerBetweenRange(0, allVerbs.size());
		int rAdv = getRandomIntegerBetweenRange(0, allAdverbs.size());
		int rPro = getRandomIntegerBetweenRange(0, allPro.size());
		
		String pronoun = (String)allPro.get(rPro);
		
		String adj1 = (String)allAdjectives.get(rAdj);
		char firstLetter = adj1.charAt(0);
		// for each search, make sure the first letter matches the first letter of last element
		
		String noun1 = (String)allNouns.get(rNum);
		char n1Letter = noun1.charAt(0);
		while(n1Letter != firstLetter) {
			
			if (rNum >= allNouns.size() - 5) {
				rNum = 0;
			}
			noun1 = (String)allNouns.get(rNum);
			n1Letter = noun1.charAt(0);
			rNum = rNum + 1;
			
		} 
		
		String adv = (String)allAdverbs.get(rAdv);
		char advLetter = adv.charAt(0);
		while(advLetter != firstLetter) {
			
			if (rAdv >= allAdverbs.size() - 5) {
				rAdv = 0;
			}
			
			adv = (String)allAdverbs.get(rAdv);
			advLetter = adv.charAt(0);
			rAdv = rAdv + 1;
		}
		
		String verb = (String)allVerbs.get(rVerb);
		char verbLetter = verb.charAt(0);
		while(verbLetter != firstLetter) {
			
			if (rVerb >= allVerbs.size() - 5) {
				rVerb = 0;
			}
			
			verb = (String)allVerbs.get(rVerb);
			verbLetter = verb.charAt(0);
			rVerb = rVerb + 1;
		}
		
		String pronoun2 = (String)allPro.get(rPro / 4);
		char proLetter2 = pronoun2.charAt(0);
		while(proLetter2 != firstLetter) {
			
			if (rPro >= allPro.size() - 5) {
				rPro = 0;
			}
			
			pronoun2 = (String)allPro.get(rPro);
			proLetter2 = pronoun2.charAt(0);
			rPro = rPro + 1;
		}
		
		String adj2 = (String)allAdjectives.get(rAdj / 2);
		char adjLetter2 = adj1.charAt(0);
		while(adjLetter2 != firstLetter) {
			
			if (rAdj >= allAdjectives.size() - 5) {
				rAdj = 0;
			}
			
			adj2 = (String)allNouns.get(rAdj);
			adjLetter2 = adj2.charAt(0);
			rAdj = rAdj + 1;
		}
		
		String noun2 = (String)allNouns.get(rNum / 3);
		char nounLetter2 = noun2.charAt(0);
		while(nounLetter2 != firstLetter) {
			
			if (rNum >= allNouns.size() - 5) {
				rNum = 0;
			}
			
			noun2 = (String)allNouns.get(rNum);
			nounLetter2 = noun2.charAt(0);
			rNum = rNum + 1;
		}
		
		sentence =  pronoun + " " + adj1 + " " + noun1 + " "  + adv + " "  + verb + " "  + pronoun2 + " "  + adj2 + " "  + noun2;
		return sentence;
	}

	public static int getRandomIntegerBetweenRange(double min, double max){
	    int x = (int) ((int)(Math.random()*((max-min)+1))+min);
	    return x;
	}
	
	
}

